from flask import Flask
from flask import request, jsonify
import os, psutil
from algorithms.o_one.sum_numbers import sum_numbers
from algorithms.o_n.sum_array import sum_array
from algorithms.o_n.linear_search import search_in_array
from algorithms.o_sqrt.prime_factors import prime_factors
from algorithms.graphs.coloring import Graph as GraphColouring
from algorithms.graphs.kruskal import Graph as GraphKruskal
from algorithms.graphs.dijkstra import Graph as GraphDijkstra
from algorithms.o_square.bubble_sort import bubble_sort
from algorithms.hash.hash_cracker import main as main_hash_cracker
import tracemalloc


app = Flask(__name__)
app.config["DEBUG"] = True

ROUTE_PREFIX = '/api'

@app.route('/', methods=['GET'])
def home():
    return jsonify('Development server for collecting the data')


# O(1) complexity functions
@app.route(ROUTE_PREFIX + '/1/sum-numbers', methods=['POST'])
def api_sum_numbers():
    tracemalloc.start()

    a = int(request.form.get('a'))
    b = int(request.form.get('b'))
    response = sum_numbers(a, b)

    current, peak = tracemalloc.get_traced_memory()
    tracemalloc.stop()

    return jsonify({
        'response': response,
        'memory': current
    })

# O(n) complexity function
@app.route(ROUTE_PREFIX + '/n/sum-array', methods=['POST'])
def api_sum_array():
    tracemalloc.start()

    a = eval(request.form.get('a'))
    response = sum_array(a)

    current, peak = tracemalloc.get_traced_memory()
    tracemalloc.stop()

    return jsonify({
        'response': response,
        'memory': current
    })


@app.route(ROUTE_PREFIX + '/n/linear-search', methods=['POST'])
def api_linear_search():
    tracemalloc.start()

    a = eval(request.form.get('a'))
    x = int(request.form.get('x'))
    response = search_in_array(a, x)

    current, peak = tracemalloc.get_traced_memory()
    tracemalloc.stop()

    return jsonify({
        'response': response,
        'memory': current
    })


# O(sqrt(n)) complexity function
@app.route(ROUTE_PREFIX + '/n-sqrt/prime-factors', methods=['POST'])
def api_prime_factors():
    tracemalloc.start()

    n = int(request.form.get('n'))
    response = prime_factors(n)

    current, peak = tracemalloc.get_traced_memory()
    tracemalloc.stop()

    return jsonify({
        'response': response,
        'memory': current
    })


# O(n^2) complexity function
@app.route(ROUTE_PREFIX + '/n-square/bubble-sort', methods=['POST'])
def api_bubble_sort():
    tracemalloc.start()

    a = eval(request.form.get('a'))
    response = bubble_sort(a)

    current, peak = tracemalloc.get_traced_memory()
    tracemalloc.stop()

    return jsonify({
        'response': response,
        'memory': current
    })


# Graph algorithms
@app.route(ROUTE_PREFIX + '/graph/colouring', methods=['POST'])
def api_graph_colouring():
    tracemalloc.start()

    a = eval(request.form.get('a'))
    m = int(request.form.get('m'))
    g = GraphColouring(len(a))
    g.graph = a
    response = g.graph_colouring(m)

    current, peak = tracemalloc.get_traced_memory()
    tracemalloc.stop()

    return jsonify({
        'response': response,
        'memory': current
    })


@app.route(ROUTE_PREFIX + '/graph/kruskal', methods=['POST'])
def api_graph_kruskal():
    tracemalloc.start()

    a = eval(request.form.get('a'))
    n = eval(request.form.get('n'))

    g = GraphKruskal(n)
    for edge in a:
        g.add_edge(edge[0], edge[1], edge[2])

    response = g.kruskal_mst()

    current, peak = tracemalloc.get_traced_memory()
    tracemalloc.stop()

    return jsonify({
        'response': response,
        'memory': current
    })


@app.route(ROUTE_PREFIX + '/graph/dijkstra', methods=['POST'])
def api_graph_dijkstra():
    tracemalloc.start()

    a = eval(request.form.get('a'))
    start = int(request.form.get('start'))

    g = GraphDijkstra(len(a))
    g.graph = a

    response = g.dijkstra(start)

    current, peak = tracemalloc.get_traced_memory()
    tracemalloc.stop()

    return jsonify({
        'response': response,
        'memory': current
    })


# Hash algorithm
@app.route(ROUTE_PREFIX + '/hash-cracker', methods=['POST'])
def api_hash_cracker():
    tracemalloc.start()

    hash = str(request.form.get('hash'))
    type = request.form.get('type')

    argv = ['-h', bytes(hash, encoding='utf-8'), '-t', type, '-w', 'algorithms/hash/Wordlist.txt']
    response = main_hash_cracker(argv)

    current, peak = tracemalloc.get_traced_memory()
    tracemalloc.stop()

    return jsonify({
        'response': response,
        'memory': current
    })


# SERVER RUN
if __name__ == '__main__':
    app.run()
