from random import randint, choice
from static import gen_graph
import hashlib


def api_sum_numbers_generator(form=1, to=9000000):
    a = randint(form, to)
    b = randint(form, to)
    return a, b

def api_sum_array_generator(form=5, to=1000000):
    size = randint(form, to)
    a = []

    for _ in range(size):
        a.append(randint(form, to))

    return a

def api_linear_search_generator(form=5, to=1000000):
    size = randint(form, to)
    a = []

    for _ in range(size):
        a.append(randint(1, to))

    index = randint(0, size)
    return a, a[index]

def api_prime_factors_generator(form=1, to=999999999999999):
    return randint(form, to)

def api_bubble_sort_generator(form=5, to=15000):
    size = randint(form, to)
    a = []

    for _ in range(size):
        a.append(randint(form, to))

    return a

def api_graph_colouring_generator(form=5, to=900, m_form=5, m_to=900):
    size = randint(form, to)
    m = randint(min(m_form, size), min(m_to, size))
    matrix = [[0 for x in range(size)] for y in range(size)]

    generated_matrix, nodes, num_of_edges = api_graph_kruskal_generator(size, size)

    for edge in generated_matrix:
        matrix[edge[0]][edge[1]] = 1
        matrix[edge[1]][edge[0]] = 1

    return matrix, m, num_of_edges

def api_graph_kruskal_generator(form=5, to=9999):
    nodes = randint(form, to)
    edges = nodes * randint(7,10)

    argv = ['-grnm', '-n', str(nodes), '-m', str(edges)]
    result = gen_graph.main(argv)
    matrix = []

    for edge in result:
        matrix.append([edge[0], edge[1], randint(1, 9)])

    return matrix, nodes, len(result)

def api_graph_dijkstra_generator(form=5, to=2200):
    size = randint(form, to)
    start = randint(0, size - 1)
    matrix = [[0 for x in range(size)] for y in range(size)]

    generated_matrix, nodes, num_of_edges = api_graph_kruskal_generator(size, size)

    for edge in generated_matrix:
        matrix[edge[0]][edge[1]] = edge[2]
        matrix[edge[1]][edge[0]] = edge[2]

    return matrix, start, num_of_edges


def api_hash_generator(hash_type=None):
    types = ["md5", "sha1", "sha224", "sha256", "sha384", "sha512"]
    if hash_type is None:
        hash_type = choice(types)

    if "md5" in hash_type:
        h = hashlib.md5
    elif "sha1" in hash_type:
        h = hashlib.sha1
    elif "sha224" in hash_type:
        h = hashlib.sha224
    elif "sha256" in hash_type:
        h = hashlib.sha256
    elif "sha384" in hash_type:
        h = hashlib.sha384
    elif "sha512" in hash_type:
        h = hashlib.sha512

    password = choice(list(open('algorithms/hash/Wordlist.txt'))).replace('\n', '').replace(' ', '')
    password = h(bytes(password, encoding="utf-8")).hexdigest()

    return password, hash_type